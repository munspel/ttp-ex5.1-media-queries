# Медиа запити

**Адаптивна версання** – підхід, що передбачає зміну дизайну в залежності від поведінки користувача, розміру екрана, платформи і орієнтації девайса. Іншими словами, сторінка повинна автоматично підлаштовуватися під роздільну здатність, змінювати розмір картинок, шрифтів тощо.
Основний механізм, який дозволяє «адаптуватися» до різноманітних пристроїв є так звані медіа-запити в CSS. 

Основний механізм, який дозволяє «адаптуватися» до різноманітних пристроїв є так звані медіа-запити в CSS. 

## Атрибут Media
Медіа запити дозволяють використовувати особливі css-стилі для конкретних пристроїв виведення. 
До появи CSS3 розробники могли підключати спеціальні стилі для різних пристроїв за допомогою атрибута media, наприклад:
```
<link rel = "stylesheet" href = "print.css" media = "print">
```
**Основні типи пристроїв**

| Значення | Опис |
|----------|--------------|
| **all**	| Подходит для всех типов устройств.|
| **print**	| Предназначен для страничных материалов и документов, просматриваемых на экране в режиме предварительного просмотра печати.|
| **screen**	| Предназначен в первую очередь для экранов цветных компьютерных мониторов.|
| **speech**	| Предназначен для синтезаторов речи.|

## Контрольні точки

При створенні медіазапитів необхідно орієнтуватися на так звані контрольні точки дизайну.

До переліку основних контрольних точок можна віднести наступні:

```css
/* Smartphones (вертикальная и горизонтальная ориентация) ----------- */
@media only screen and (min-width : 320px) and (max-width : 480px) {
/* стили */
}
/* Smartphones (горизонтальная) ----------- */
@media only screen and (min-width: 321px) {
/* стили */
}
/* Smartphones (вертикальная) ----------- */
@media only screen and (max-width: 320px) {
/* стили */
}
/* iPads (вертикальная и горизонтальная) ----------- */
@media only screen and (min-width: 768px) and (max-width: 1024px) {
/* стили */
}
/* iPads (горизонтальная) ----------- */
@media only screen and (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
/* стили */
}
/* iPads (вертикальная) ----------- */
@media only screen and (min-width: 768px) and (max-width: 1024px) and (orientation: portrait) {
/* стили */
}
/* iPad 3**********/
@media only screen and (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
/* стили */
}
@media only screen and (min-width: 768px) and (max-width: 1024px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {
/* стили */
}
/* Настольные компьютеры и ноутбуки ----------- */
@media only screen  and (min-width: 1224px) {
/* стили */
}
/* Большие экраны ----------- */
@media only screen  and (min-width: 1824px) {
/* стили */
}
/* iPhone 4 ----------- */
@media only screen and (min-width: 320px) and (max-width: 480px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
/* стили */
}
@media only screen and (min-width: 320px) and (max-width: 480px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {
/* стили */
}
/* iPhone 5 ----------- */
@media only screen and (min-width: 320px) and (max-height: 568px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
@media only screen and (min-width: 320px) and (max-height: 568px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
/* iPhone 6 ----------- */
@media only screen and (min-width: 375px) and (max-height: 667px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
@media only screen and (min-width: 375px) and (max-height: 667px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
/* iPhone 6+ ----------- */
@media only screen and (min-width: 414px) and (max-height: 736px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
@media only screen and (min-width: 414px) and (max-height: 736px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
/* Samsung Galaxy S3 ----------- */
@media only screen and (min-width: 320px) and (max-height: 640px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
@media only screen and (min-width: 320px) and (max-height: 640px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2){
/* стили */
}
/* Samsung Galaxy S4 ----------- */
@media only screen and (min-width: 320px) and (max-height: 640px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 3){
/* стили */
}
@media only screen and (min-width: 320px) and (max-height: 640px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 3){
/* стили */
}
/* Samsung Galaxy S5 ----------- */
@media only screen and (min-width: 360px) and (max-height: 640px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 3){
/* стили */
}
@media only screen and (min-width: 360px) and (max-height: 640px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 3){
/* стили */
}
```
## Корисні посилання

- https://html5book.ru/css3-mediazaprosy/
